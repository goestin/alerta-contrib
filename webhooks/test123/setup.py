from setuptools import setup, find_packages

version = '1.0.0'

setup(
    name="alerta-test123",
    version=version,
    description='Alerta Webhook for test123',
    url='https://gitlab.com/goestin/alerta-contrib',
    license='MIT',
    author='Goes Sepouse',
    author_email='goestin@intert00bz.nl',
    packages=find_packages(),
    py_modules=['alerta_test123'],
    install_requires=[],
    include_package_data=True,
    zip_safe=True,
    entry_points={
        'alerta.webhooks': [
            'test123 = alerta_test123:Test123Webhook'
        ]
    }
)
